import { Client } from './../../model/Client';
import { FlashMessagesService } from 'angular2-flash-messages';
import { ClientService } from './../../services/client.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';


@Component({
  selector: 'app-client-details',
  templateUrl: './client-details.component.html',
  styleUrls: ['./client-details.component.css']
})
export class ClientDetailsComponent implements OnInit {

  id:string;
  client: Client;
  hasBalance: boolean = false;
  showBalanceUpdateInput:boolean = false;

  constructor(
    public clientService: ClientService,
    public router: Router,
    public route: ActivatedRoute,
    public flashMessagesService: FlashMessagesService 
  ) { }

  ngOnInit() {

    this.id = this.route.snapshot.params['id'];
    //Get Client
    this.clientService.getClient(this.id).subscribe(client =>{
      if (client.balance > 0){
        this.hasBalance = true;
      }
      this.client = client;
    })
  }

  updateBalance(id:string){
    this.clientService.updateClient(this.id, this.client);
    this.showBalanceUpdateInput = !this.showBalanceUpdateInput;
    this.flashMessagesService.show('Balance updated', {cssClass:'alert-success', timeout:4000});
    this.router.navigate(['details/'+id]);
  }

  onDeleteClick(){
    if(confirm("Are you sure to delete ?")){
      this.clientService.deleteClient(this.id);
      this.flashMessagesService.show('Client deleted', {cssClass:'alert-success', timeout:4000});
      this.router.navigate(['/']);
    }
  }

}
