import { ClientService } from './../../services/client.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Client } from './../../model/Client';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SettingsService } from '../../services/settings.service';


@Component({
  selector: 'app-add-client',
  templateUrl: './add-client.component.html',
  styleUrls: ['./add-client.component.css']
})
export class AddClientComponent implements OnInit {

  client: Client = {
    firstName:'',
    lastName:'',
    email:'',
    phone:'',
    balance:0
    
  };

  disableBalanceOnAdd: boolean = true;

  constructor(
    public clientService: ClientService,
    public flashMessgaesServices: FlashMessagesService,
    public router: Router,
    public settingService: SettingsService
  ) { }

  ngOnInit() {
    this.disableBalanceOnAdd = this.settingService.getSettings().disableBalanceOnAdd;
  }

  onSubmit({value, valid}:{value:Client, valid:boolean}){
    if(this.disableBalanceOnAdd){
      value.balance =0;
    }
    if (!valid){
      this.flashMessgaesServices.show('Please fill in all fields', {cssClass:'alert-danger', timeout:4000});
      this.router.navigate(['add-client']);
    }else{
      this.clientService.newClient(value);
      this.flashMessgaesServices.show('New Client added', {cssClass:'alert-success', timeout:4000});
      this.router.navigate(['/']);
    }
  }

}
