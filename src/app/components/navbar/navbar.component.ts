import { SettingsService } from './../../services/settings.service';
import { Settings } from './../../model/settings';
import { Observable } from 'rxjs';
import { AuthService } from './../../services/auth.service';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  isLoggedIn: boolean = true;
  loggedInUser:string;
  showRegister:boolean;

  constructor(
    private authService: AuthService,
    private route: Router,
    private flashMessagesService : FlashMessagesService,
    private settingsService: SettingsService
  ) { }

  ngOnInit() {

    this.authService.getAuth().subscribe(auth => {
      if (auth){
        this.isLoggedIn = true;
        this.loggedInUser = auth.email;
      }else{
        this.isLoggedIn = false;
      }
      this.showRegister = this.settingsService.getSettings().allowRegistration;
    });
  }

  onLogoutClick(){
    this.authService.logout();
    this.flashMessagesService.show('You are logged out', {cssClass:'alert-success', timeout:4000});
    this.route.navigate(['/login']);
  }

}
