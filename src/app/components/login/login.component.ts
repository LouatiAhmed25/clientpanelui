import { AuthService } from './../../services/auth.service';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email:string;
  password:string;

  constructor(
    private route: Router,
    private flashMessagesService: FlashMessagesService,
    private authService: AuthService
  ) { }

  ngOnInit() {
    
  }

  onSubmit(){
    this.authService.login(this.email,this.password)
      .then((res) => {
        this.flashMessagesService.show('You are logged in', {cssClass:'alert-success', timeout:4000});
        this.route.navigate(['/']);
      })
      .catch((err) => {
        this.flashMessagesService.show(err.message , {cssClass:'alert-danger', timeout:4000});
        this.route.navigate(['/login']);
      });
  }

}
