import { SettingsService } from './../services/settings.service';
import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';



@Injectable()

export class RegisterGuard implements CanActivate{

    constructor(
        private route: Router,
        public settingsService: SettingsService
    ){}
    

    canActivate() : boolean {
        if (this.settingsService.getSettings().allowRegistration)
        return true;
        else {
            this.route.navigate(['/login']) ;
            return false;
        }
    }
}
